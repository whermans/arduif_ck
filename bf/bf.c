/*
 Created by Wouter Hermans on 18/07/2019.
*/

#include "bf.h"
/*
++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.
 */

State bf_init()
{
    State state;
    state.ptr = 0;
    memset(state.cells, 0, CELLS_SZ);
    return state;
}

unsigned char bf_run(State *state)
{
    char buffer[BUFF_SZ];
    io_read_s(BUFF_SZ, buffer);
    return bf_parse(state, buffer);
}

unsigned char bf_parse(State *state, const char *buffer)
{
    int i;
    unsigned char failed;
    failed = 0;
    for(i = 0; i < BUFF_SZ && !failed && buffer[i] != 10; i++)
    {
        if(buffer[i] == BF_PTR_NXT)
        {
            failed = bf_ptr_nxt(state);
        }
        else if(buffer[i] == BF_PTR_PRV)
        {
            failed = bf_ptr_prev(state);
        }
        else if(buffer[i] == BF_INC)
        {
            failed = bf_inc(state);
        }
        else if(buffer[i] == BF_DEC)
        {
            failed = bf_dec(state);
        }
        else if(buffer[i] == BF_OUT)
        {
            failed = bf_out(state);
        }
        else if(buffer[i] == BF_IN)
        {
            failed = bf_in(state);
        }
        else if(buffer[i] == BF_JMP_FWD)
        {
            failed = bf_jmp_fwd(state, buffer, &i);
        }
        else if(buffer[i] == BF_JMP_BCK)
        {
            failed = bf_jmp_bck(state, buffer, &i);
        }else{
            failed = 1;
        }
    }
    return failed;
}

unsigned char bf_ptr_nxt(State *state)
{
    state->ptr++;
    if(state->ptr >= CELLS_SZ)
    {
        return 1;
    }
    return 0;
}

unsigned char bf_ptr_prev(State *state)
{
    state->ptr--;
    if(state->ptr < 0)
    {
        return 1;
    }
    return 0;
}

unsigned char bf_inc(State *state)
{
    if((state->cells[state->ptr]) == CHAR_MAX)
    {
        return 1;
    }
    (state->cells[state->ptr])++;
    return 0;
}

unsigned char bf_dec(State *state)
{
    if((state->cells[state->ptr]) == 0)
    {
        return 1;
    }
    (state->cells[state->ptr])--;
    return 0;
}

unsigned char bf_out(State *state)
{
    io_write(state->cells[state->ptr]);
    return 0;
}

unsigned char bf_in(State *state)
{
    state->cells[state->ptr] = io_read();
    return 0;
}

unsigned char bf_jmp_fwd(State *state, const char *buffer, int *i)
{
    if(!state->cells[state->ptr])
    {
        while((*i)++ < BUFF_SZ - 1)
        {
            if(buffer[*i] == BF_JMP_BCK)
            {
                return 0;
            }
        }
    }
    return 0;
}

unsigned char bf_jmp_bck(State *state, const char *buffer, int *i)
{
    if(state->cells[state->ptr])
    {
        unsigned char skip;
        skip = 0;
        while((*i)-- > 0)
        {
            if(buffer[*i] == BF_JMP_FWD)
            {
                if(skip == 0){
                    return 0;
                }else{
                    skip--;
                }
            }else if(buffer[*i] == BF_JMP_BCK)
            {
                skip++;
            }
        }
    }
    return 0;
}
