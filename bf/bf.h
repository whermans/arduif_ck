/*
 Created by Wouter Hermans on 18/07/2019.
*/

#ifndef BRAINFUCK_BF_H
#define BRAINFUCK_BF_H

#include "../io/io.h"
#include <string.h>
#include <limits.h>

#define BF_PTR_NXT '>'
#define BF_PTR_PRV '<'
#define BF_INC '+'
#define BF_DEC '-'
#define BF_OUT '.'
#define BF_IN ','
#define BF_JMP_FWD '['
#define BF_JMP_BCK ']'

#define CELLS_SZ 128
#define BUFF_SZ 255

typedef struct {
    int ptr;
    char cells[CELLS_SZ];
} State;

State bf_init(void);
unsigned char bf_run(State *state);
unsigned char bf_parse(State *state, const char *buffer);
unsigned char bf_ptr_nxt(State *state);
unsigned char bf_ptr_prev(State *state);
unsigned char bf_inc(State *state);
unsigned char bf_dec(State *state);
unsigned char bf_out(State *state);
unsigned char bf_in(State *state);
unsigned char bf_jmp_fwd(State *state, const char *buffer, int *i);
unsigned char bf_jmp_bck(State *state, const char *buffer, int *i);

#endif
