/*
 Created by Wouter Hermans on 18/07/2019.
*/

#ifndef BRAINFUCK_IO_H
#define BRAINFUCK_IO_H

void io_write(char c);
char io_read(void);
void io_read_s(int sz, char *buffer);

#endif
