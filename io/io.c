/*
 Created by Wouter Hermans on 18/07/2019.
*/

#include "io.h"
#include "../uart/uart.h"

void io_write(char c)
{
    uart_tx_c(c);
}

char io_read(void)
{
    return uart_rx_c();
}

void io_read_s(int sz, char *buffer)
{
    int i;
    char c;
    i = 0;
    c = 0;
    while(i < sz && c != 10)
    {
        c = io_read();
        buffer[i++] = c;
    }
}

