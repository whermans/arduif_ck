#include "uart/uart.h"
#include "bf/bf.h"

void setup(void);

void loop(void);

int main()
{
    setup();
    for (;;)
    {
        loop();
    }
}

void setup()
{
    uart_init(9600);
}

void loop()
{
    State state;
    uart_tx_s("Code?\n");
    state = bf_init();
    bf_run(&state);
    uart_tx_s("\nDone\n");
}
