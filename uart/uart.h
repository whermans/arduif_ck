/*
 Created by Wouter Hermans on 09/07/2019.
*/


#ifndef ARDUINO_UART_H
#define ARDUINO_UART_H

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>

uint8_t uart_init(uint16_t baud_rate);
uint8_t uart_tx_c(uint8_t data);
uint8_t uart_tx_s(char *data);
uint8_t uart_rx_c(void);

#endif
