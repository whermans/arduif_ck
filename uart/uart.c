#include "uart.h"

uint8_t uart_init(uint16_t baud_rate)
{
    /* Adjust baud rate to clock speed */
    uint16_t adj_baud_rate = (F_CPU / 16) / (baud_rate - 1);

    /* Set high register */
    UBRR0H = adj_baud_rate >> 8;
    /* Set low register*/
    UBRR0L = adj_baud_rate;

    /* Set control and status register B: TX enable, RX enable, RX complete interrupt enable */
    UCSR0B = (1 << TXEN0) | (1 << RXEN0) | (1 << RXCIE0);
    /* Set control and status register C: 1 stop bit, 6(?) character bits*/
    UCSR0C = (1 << USBS0) | (3 << UCSZ00);
    return 0;
}

uint8_t uart_tx_c(uint8_t data)
{
    /* Idle until TX buffer is ready */
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = data;
    return 0;
}

uint8_t uart_tx_s(char *data)
{
    /* Loop over data and send each byte*/
    while (*data)
    {
        uart_tx_c(*data);
        data++;
    }
    return 0;
}

uint8_t uart_rx_c(void)
{
    /* Idle until RX buffer is ready */
    while (!(UCSR0A & (1 << RXC0)));
    return UDR0;
}

